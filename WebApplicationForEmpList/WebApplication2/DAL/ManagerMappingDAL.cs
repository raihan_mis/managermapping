﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplication2.DataManager;
using WebApplication2.Models;

namespace WebApplication2.DAL
{
    public class ManagerMappingDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public DataTable GetManagerWiseEmpList(int empId)
        {
            _accessManager.SqlConnectionOpen();
            try
            {
                List<Employee> employeeList = new List<Employee>();
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@empId", empId));
                DataTable dt = _accessManager.GetDataTable("sp_FindManagementChain", parameters);
                return dt;
               
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}