﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.DAL;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class ManagerMappingController : Controller
    {
        ManagerMappingDAL _managerMappingDAL = new ManagerMappingDAL();
        // GET: ManagerMapping
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetManagerWiseEmpList(int empId)
        {
           DataTable employeeList = _managerMappingDAL.GetManagerWiseEmpList(empId);
          
            return PartialView("_EmpListPartialView", employeeList);
        }
    }
}