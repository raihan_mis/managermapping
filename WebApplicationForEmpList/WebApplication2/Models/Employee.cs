﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime? JoinDate { get; set; }

        public string Designation { get; set; }

        public decimal? Salary { get; set; }

        public bool? IsBonusAdded { get; set; }

        public int? SuperiorId { get; set; }
    }
}