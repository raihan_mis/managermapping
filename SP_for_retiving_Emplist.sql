-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE sp_FindManagementChain  -- sp_FindManagementChain 16
	@empId int
AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		declare @rootId int ,@rowNu int;

	------------- At first, we need to get the root node:recursive to find the root ---------------------

		WITH findRoot(row_num,rootD) 
		AS (
      
		  select  1,superiorid from Employee where id=@empId
		        UNION ALL
		    SELECT row_num+1,
		      superiorid
		    FROM Employee emp 
		    INNER JOIN findRoot fr 
			  ON emp.id = fr.rootD 
		)
		SELECT top 1 @rowNu=row_num,
		  @rootId=rootD
		FROM 
		    findRoot 
		    where rootD!=0
		    order by row_num desc;

           ------------ we recursively go deeper to find the child:recursive to find the child from root ------------------

		
		WITH findChildData(empId,empName,position,SalBonus,JoiningDate) 
		AS (
		   select id,[Name],Designation,Salary,JoinDate from Employee where id=@rootId
		    UNION ALL
        
		    SELECT
		      id,[Name],Designation,Salary,JoinDate
		    FROM Employee emp1 
		    INNER JOIN findChildData cd 
			  ON emp1.superiorid = cd.empId
		)
		SELECT 
		 empId,empName,position,SalBonus,Convert(nvarchar(20),JoiningDate,105)JoiningDate
		FROM 
		    findChildData;
END
GO
